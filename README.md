# Github contributors rating

## Initialize for development

- Go to project folder
- Open terminal
- Execute ``yarn install``
- Execute ``cp .env.example .env``
- Fill .env fle with all values
- Execute ``yarn start``

## Make production build

- Go to project folder
- Open terminal
- Execute ``yarn install``
- Execute ``cp .env.example .env``
- Fill .env fle with all values
- Execute ``yarn build``
