import { DEFAULT_LANG } from 'constants.js';
import { config } from 'lang/config';

const getLocale = (lang) => (config[lang] && lang) || DEFAULT_LANG;

export default getLocale;
