import { DEFAULT_LANG, STORAGE_LANG } from 'constants.js';
import LocalStorage from 'service/LocalStorage';
import RuMessages from './ru-RU';
import EnMessages from './en-EN';

export const config = {
  ru: {
    messages: RuMessages,
    name: 'Русский',
  },
  en: {
    messages: EnMessages,
    name: 'English',
  },
};

export const langOptions = Object.keys(config).map((key) => ({
  value: key,
  name: config[key].name,
}));

export const getMessages = (locale) => {
  const langObj = config[locale];
  if (!langObj) {
    LocalStorage.remove(STORAGE_LANG);
    const langObject = config[navigator.language] || config[DEFAULT_LANG];
    return langObject.messages;
  }

  return langObj.messages;
};
