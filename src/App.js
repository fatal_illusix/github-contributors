import React from 'react';
import { useSelector } from 'react-redux';
import Router from 'components/router';
import { IntlProvider } from 'react-intl';
import { createTheme, ThemeProvider } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';

import { getMessages } from 'lang/config';
import { darkTheme, lightTheme } from 'themes';
import getLocale from 'utils/helpers';

import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';

const App = () => {
  const { lang, isDarkTheme } = useSelector((state) => state.general);
  const messages = getMessages(lang);
  const theme = createTheme(isDarkTheme ? darkTheme : lightTheme);

  return (
    <IntlProvider locale={getLocale(lang)} messages={messages}>
      <ThemeProvider theme={theme}>
        <CssBaseline/>
          <Router />
      </ThemeProvider>
    </IntlProvider>
  );
};

export default App;
