import React, { useState } from 'react';
import Popover from '@material-ui/core/Popover';
import Slider from 'components/fields/slider';
import { makeStyles } from '@material-ui/core/styles';
import { FormattedMessage } from 'react-intl';

const useStyles = makeStyles(() => ({
  popoverHintContainer: {
    height: 0,
    overflow: 'visible',
  },
  popoverHint: {
    fontSize: '0.8em',
    color: 'grey',
    lineHeight: 1,
  },
}));

// eslint-disable-next-line react/display-name
const withFilterInPopover = (Component) => (props) => {
  const {
    handleClose, children, activeFilters, maxPerFields, name, ...componentProps
  } = props;

  const [anchorEl, setAnchorEl] = useState(null);
  const value = activeFilters[name];
  const maxValue = maxPerFields[name];
  const [localValue, setLocalValue] = useState(value ? [value.min, value.max] : [0, maxValue]);

  const onFilterChange = (val) => {
    setLocalValue(val);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  const classes = useStyles();

  return (
    <>
      <Component
        aria-describedby={id}
        color="primary"
        onClick={handleClick}
        {...componentProps}
      >
        {children}
        <div className={classes.popoverHintContainer}>
          <div className={classes.popoverHint}>
            (<FormattedMessage id='openFilter'/>)
          </div>
        </div>
      </Component>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={() => {
          setAnchorEl(null);
          handleClose(name, localValue);
        }}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <Slider name={name} value={localValue} maxValue={maxValue} onChange={onFilterChange} />
      </Popover>
    </>
  );
};

export default withFilterInPopover;
