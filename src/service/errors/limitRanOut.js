import BaseError from './base';

class LimitRanOutError extends BaseError {}

export default LimitRanOutError;
