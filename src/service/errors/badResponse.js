import BaseError from './base';

class BadResponseError extends BaseError {}

export default BadResponseError;
