class BaseError extends Error {
  constructor(message, payload) {
    super(message);
    if (payload) {
      this.payload = payload;
    }
  }
}

export default BaseError;
