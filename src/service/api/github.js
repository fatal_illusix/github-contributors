import {
  BASE_URL,
  PERSONAL_ACCESS_TOKEN,
  ORG_NAME,
} from 'configs.js';
import { DEFAULT_PER_PAGE } from 'constants.js';
import Request from './request';

class GithubApi {
  constructor() {
    this.baseUrl = BASE_URL;
    this.token = `token ${PERSONAL_ACCESS_TOKEN}`;
    this.organizationName = ORG_NAME;
  }

  getUrl(url, pagination = {}) {
    const paramsArr = [];
    const { perPage, page } = pagination;
    if (perPage) {
      paramsArr.push(`per_page=${perPage}`);
    }
    if (page) {
      paramsArr.push(`page=${page}`);
    }
    return `${this.baseUrl}${url}${paramsArr.length ? `?${paramsArr.join('&')}` : ''}`;
  }

  async getUserData(userName) {
    const url = this.getUrl(`users/${userName}`);
    return Request.get(url, this.token);
  }

  async getContributorsByOrgRepo(repository, organizationName = this.organizationName, page = 1) {
    const url = this.getUrl(`repos/${organizationName}/${repository}/contributors`, { perPage: DEFAULT_PER_PAGE, page });
    return Request.get(url, this.token);
  }

  async getReposByOrg(repository, page = 1) {
    const url = this.getUrl(`orgs/${this.organizationName}/repos`, { perPage: DEFAULT_PER_PAGE, page });
    return Request.get(url, this.token);
  }

  async getReposByUser(userName, page = 1) {
    const url = this.getUrl(`users/${userName}/repos`, { perPage: DEFAULT_PER_PAGE, page });
    return Request.get(url, this.token);
  }
}

export default new GithubApi();
