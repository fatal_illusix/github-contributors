import axios from 'axios';
import { REMAINING_LIMIT_HEADER } from '../../constants';
import LimitRanOutError from '../errors/limitRanOut';
import BadResponseError from '../errors/badResponse';

class Request {
  static getHeaders(auth) {
    const baseHeaders = { 'content-type': 'application/json' };
    return {
      headers: auth ? Object.assign(baseHeaders, { Authorization: auth }) : baseHeaders,
    };
  }

  static processResponseHeaders(headers = {}) {
    const remainingLimit = parseInt(headers[REMAINING_LIMIT_HEADER], 10);

    if (Number.isNaN(remainingLimit)) {
      throw new BadResponseError('Request:processResponseHeaders:remainingLimitIsNan');
    }

    if (remainingLimit > 0) {
      return;
    }

    throw new LimitRanOutError('Request:processResponseHeaders:limitRanOut');
  }

  static processAnswer(results = {}) {
    const {
      data, headers, status, statusText,
    } = results;

    if (!data || !headers || typeof data !== 'object' || typeof headers !== 'object') {
      throw new BadResponseError('Request:processAnswer:wrongResponse');
    }
    if (Math.round(status / 100) > 2) {
      throw new BadResponseError('Request:processAnswer:responseStatusOver200', { status, statusText });
    }

    Request.processResponseHeaders(headers);

    return data;
  }

  static async get(url, auth) {
    return axios.get(url, Request.getHeaders(auth))
      .then(Request.processAnswer);
  }
}

export default Request;
