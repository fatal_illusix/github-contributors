class LocalStorage {
  static set(key, data) {
    let convertedData = '';
    try {
      convertedData = typeof data === 'object' || typeof data === 'boolean' ? JSON.stringify(data) : data;
    } catch (error) {
      console.error(error);
    }

    localStorage.setItem(key, convertedData);
  }

  static get(key) {
    return localStorage.getItem(key);
  }

  static remove(key) {
    localStorage.removeItem(key);
  }
}

export default LocalStorage;
