export default {
  typography: {
    button: {
      textTransform: 'none',
    },
  },
  overrides: {
    MuiFormControlLabel: {
      labelPlacementStart: {
        marginLeft: 0,
      },
    },
    MuiCircularProgress: {
      colorSecondary: {
        color: '#921a1a',
      },
    },
    MuiSelect: {
      select: {
        '&:focus': {
          backgroundColor: 'none',
        },
      },
    },
  },
};
