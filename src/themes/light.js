import base from './base';

export default {
  ...base,
  palette: {
    type: 'light',
    primary: {
      main: '#465ead',
      dark: '#3F559E',
    },
    secondary: {
      light: '#E5E5E5',
      main: '#DCDCDC',
      dark: '#c7c7c7',
    },
    separator: {
      main: '#F3F3F3',
    },
  },
  overrides: {
    ...base.overrides,
    MuiIconButton: {
      root: {
        color: '#575757',
      },
    },
  },
};
