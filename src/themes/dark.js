import base from './base';

export default {
  ...base,
  palette: {
    type: 'dark',
    secondary: {
      main: '#737373',
    },
    separator: {
      main: '#4C4C4C',
    },
  },
};
