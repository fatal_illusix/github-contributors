import { Container } from '@material-ui/core';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgressWithLabel from '../circular-progress';

const useStyles = makeStyles(() => ({
  centeredLoaderContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'fixed',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
  },
  loaderContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

const Loader = ({
  isLoading, progressValue, centered, error, children,
}) => {
  const classes = useStyles();
  if (isLoading || error) {
    return (
      <Container className={centered ? classes.centeredLoaderContainer : classes.loaderContainer} >
        <CircularProgressWithLabel value={progressValue || 0} color={error ? 'secondary' : 'primary'} />
      </Container>
    );
  }
  return (
    <>
      {children}
    </>
  );
};

export default Loader;
