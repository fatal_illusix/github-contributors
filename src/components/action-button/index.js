import React from 'react';
import { Button } from '@material-ui/core';

const ActionButton = ({ buttonText, onClick = () => {} }) => (
    <Button size="small" onClick={onClick}>
      {buttonText}
    </Button>
);

export default ActionButton;
