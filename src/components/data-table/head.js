import React from 'react';
import { TableCell, TableHead, TableRow } from '@material-ui/core';
import withFilterInPopover from 'hocs/withFilterInPopover';
import { FILTERABLE_FIELDS } from '../../constants';

const isFilterable = (fieldName) => FILTERABLE_FIELDS
  .some((filterableFieldName) => filterableFieldName === fieldName);

const Head = (props) => {
  const {
    fields = [],
    onFilterAccept = () => {},
    activeFilters,
    maxPerFields,
  } = props;

  return (
    <TableHead>
      <TableRow>
        {fields.map(({ name, field }, i) => {
          const TableCellElement = isFilterable(field) ? withFilterInPopover(TableCell) : TableCell;
          return (
            <TableCellElement
              aria-describedby={name}
              key={field}
              component="th"
              name={field}
              scope="column"
              {...(i !== 0 ? {
                style: { width: 160, cursor: 'pointer' },
                align: 'right',
                activeFilters,
                maxPerFields,
                handleClose: onFilterAccept,
              } : {})}
            >
              {name}
            </TableCellElement>
          );
        })}
      </TableRow>
    </TableHead>
  );
};

export default React.memo(Head);
