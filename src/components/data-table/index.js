import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { injectIntl } from 'react-intl';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableFooter,
  TablePagination,
  TableRow,
  Paper,
} from '@material-ui/core';
import Pagination from 'components/pagination';
import Head from './head';

const useStyles = makeStyles({
  table: {
    minWidth: 500,
  },
});

const DataTable = (props) => {
  const classes = useStyles();
  const {
    perPage = 10,
    data = [],
    fields,
    activeFilters,
    maxPerFields,
    onRowClick = () => {},
    onFilterAccept = () => {},
    intl,
  } = props;
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(perPage);

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} stickyHeader aria-label={intl.formatMessage({ id: 'dataTable' })}>
        <Head
          fields={fields}
          activeFilters={activeFilters}
          maxPerFields={maxPerFields}
          onFilterAccept={onFilterAccept}
        />
        <TableBody>
          {(rowsPerPage > 0
            ? data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            : data
          ).map((row) => (
            <TableRow key={fields.map(({ field }) => `${row[field]}`).join('')} tabIndex={-1} hover onClick={() => onRowClick(row)} style={{ cursor: 'pointer' }}>
              {fields.map(({ field }, i) => (
                <TableCell key={field} {...(i === 0 ? { component: 'th', scope: 'row' } : { style: { width: 160 }, align: 'right' })}>
                  {row[field]}
                </TableCell>
              ))}
            </TableRow>
          ))}

          {emptyRows > 0 && (
            <TableRow style={{ height: 53 * emptyRows }}>
              <TableCell colSpan={6} />
            </TableRow>
          )}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[perPage, perPage * 1.5, perPage * 2, { label: intl.formatMessage({ id: 'all' }), value: -1 }]}
              count={data.length}
              rowsPerPage={rowsPerPage}
              page={page}
              SelectProps={{
                inputProps: { 'aria-label': intl.formatMessage({ id: 'rowsPerPage' }) },
                native: true,
              }}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
              ActionsComponent={Pagination}
            />
          </TableRow>
        </TableFooter>
      </Table>
    </TableContainer>
  );
};

export default injectIntl(DataTable);
