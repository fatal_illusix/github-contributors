import React from 'react';
import { Box } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import { makeStyles } from '@material-ui/core/styles';
import { FormattedMessage } from 'react-intl';
import DescriptionRow from './description-row';

const cardData = [
  {
    text: <FormattedMessage id='name'/>,
    field: 'login',
  },
  {
    text: <FormattedMessage id='company'/>,
    field: 'company',
  },
  {
    text: <FormattedMessage id='location'/>,
    field: 'location',
  },
  {
    text: <FormattedMessage id='bio'/>,
    field: 'bio',
  },
  {
    text: <FormattedMessage id='beHired'/>,
    field: 'hireable',
    positive: AddIcon,
    negative: RemoveIcon,
    isCompare: true,
  },
  {
    text: <FormattedMessage id='repositoriesCreated'/>,
    field: 'publicRepos',
  },
  {
    text: <FormattedMessage id='gistsCreated'/>,
    field: 'publicGists',
  },
  {
    text: <FormattedMessage id='peopleFollowing'/>,
    field: 'followers',
  },
];

const useStyles = makeStyles((theme) => ({
  userCard: {
    display: 'flex',
    padding: theme.spacing(3),
  },
  userImg: {
    marginRight: theme.spacing(2),
  },
}));

const UserCard = ({ userObject }) => {
  const classes = useStyles();

  return (
    (
      <Box className={classes.userCard} boxShadow={3}>
        <div className={classes.userImg}>
          <img src={userObject.avatarUrl} alt={userObject.name} />
        </div>
        <div className={classes.userDesc}>
          {cardData.map((settings) => {
            const { field, text } = settings;
            const value = userObject[field];

            return <DescriptionRow key={field} label={text} value={value} />;
          })}
        </div>
      </Box>
    )
  );
};

export default UserCard;
