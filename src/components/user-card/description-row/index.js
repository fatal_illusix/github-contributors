import React from 'react';
import { alpha, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  descItem: {
    display: 'flex',
    alignItems: 'center',
    '&:not(:last-child)': {
      paddingBottom: 3,
      marginBottom: 5,
      borderBottom: `1px dashed ${alpha(theme.palette.text.secondary, 0.2)}`,
    },
    fontSize: 16,
  },
  descItemTitle: {
    fontWeight: 'bold',
    marginRight: 5,
  },
}));

const DescriptionRow = (props) => {
  const { label, value } = props;
  const classes = useStyles();

  if (!value) {
    return null;
  }

  return (
    <div className={classes.descItem}>
      <div className={classes.descItemTitle}>{label}:</div>
      <div>{value}</div>
    </div>
  );
};

export default DescriptionRow;
