import React from 'react';
import {
  BrowserRouter, Route, Switch,
} from 'react-router-dom';
import { injectIntl } from 'react-intl';
import { Box, Container } from '@material-ui/core';
import HomePage from '../../pages/home';
import UserPage from '../../pages/userPage';
import ErrorContainer from '../error-container';
import Header from '../header';
import { ERROR_CONTAINER_TYPE_ENUM } from '../../constants';

const Router = ({ intl }) => (
    <BrowserRouter>
      <Header/>
      <Container>
        <Box my={2}>
          <Switch>
            <Route path="/" exact>
              <HomePage />
            </Route>
            <Route path="/user/:user" exact>
              <UserPage />
            </Route>
            <Route path="/">
              <ErrorContainer
                error={new Error('Unexisting Page')}
                optionalText={intl.formatMessage({ id: 'errorPageNotExist' })}
                buttonText={intl.formatMessage({ id: 'errorPageNotExistButtonText' })}
                type={ERROR_CONTAINER_TYPE_ENUM.goHome}
              />
            </Route>
          </Switch>
        </Box>
      </Container>
    </BrowserRouter>
);

export default injectIntl(Router);
