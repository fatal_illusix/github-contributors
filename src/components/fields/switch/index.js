import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Switch, FormControlLabel, Typography } from '@material-ui/core';
import classNames from 'classnames';

const useStyles = makeStyles(() => ({
  iconLabel: {
    display: 'flex',
    alignItems: 'center',
  },
  icon: {
    marginRight: 8,
  },
  fullWidth: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'space-between',
  },
}));

const SwitchButton = (props) => {
  const {
    onChange,
    labelPosition = 'start',
    color = 'primary',
    value, children: text,
    Icon,
    fullWidth,
  } = props;

  const classes = useStyles();

  return (
    <FormControlLabel
      className={classNames({ [classes.fullWidth]: fullWidth })}
      labelPlacement={labelPosition}
      control={
        <Switch
          onChange={onChange}
          color={color}
          checked={value}
        />
      }
      label={(
        <div className={classNames({ [classes.iconLabel]: Icon })}>
          { Icon && <Icon className={classes.icon} /> }
          <Typography variant="body2">
            {text}
          </Typography>
        </div>
      )}
    />
  );
};

export default SwitchButton;
