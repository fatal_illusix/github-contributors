import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Select, FormControl, MenuItem, FormHelperText, InputLabel,
} from '@material-ui/core';
import classNames from 'classnames';

const useStyles = makeStyles((theme) => ({
  icon: {
    position: 'absolute',
    left: 0,
    top: '50%',
    transform: 'translateY(-3px)',
  },
  fullWidthSelect: {
    flex: 1,
    fontSize: theme.typography.body2.fontSize,
  },
  inputWrapper: {
    display: 'flex',
    alignItems: 'center',
  },
  withError: {
    '& $icon': {
      transform: 'translateY(-14px)',
    },
  },
  withIcon: {
    paddingLeft: 33,
    '& label': {
      left: 7,
    },
  },
}));

const SelectBox = (props) => {
  const {
    onChange,
    Icon,
    fullWidth,
    value,
    label,
    items = [],
    error,
    name,
  } = props;

  const classes = useStyles();

  return (
    <FormControl
      error={Boolean(error)}
      fullWidth={fullWidth}
      className={classNames({ [classes.withIcon]: Icon, [classes.withError]: Boolean(error) })}
    >
      { Icon && <Icon className={classes.icon}/> }
      { label && (
        <InputLabel className={classNames({ [classes.withIcon]: Icon })} >{label}</InputLabel>
      ) }
      <Select
        name={name}
        MenuProps={{
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'left',
          },
          transformOrigin: {
            vertical: 'top',
            horizontal: 'left',
          },
          getContentAnchorEl: null,
        }}
        onChange={
          ({
            target: { name: targetName, value: targetValue },
          }) => onChange(targetName, targetValue)
        }
        className={classNames({ [classes.fullWidthSelect]: fullWidth })}
        value={value}>
        { items.map(({ name: itemName, value: itemValue }) => (
          <MenuItem value={itemValue} key={itemValue}>{itemName}</MenuItem>
        )) }
      </Select>
      { error && (
          <FormHelperText>{error}</FormHelperText>
      ) }
    </FormControl>
  );
};

export default SelectBox;
