import React from 'react';
import Typography from '@material-ui/core/Typography';
import { Button, Slider as MaterialSlider } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import { unsetFilter } from 'store/actions/users';

const useStyles = makeStyles((theme) => ({
  slider: {
    width: 300,
    padding: theme.spacing(2),
  },
  sliderHeader: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  sliderHeaderText: {
    marginBottom: 0,
  },
}));

const Slider = (props) => {
  const {
    value, maxValue, onChange, name,
  } = props;
  const classes = useStyles();

  const handleChange = (_, newValue) => {
    onChange(newValue);
  };

  const dispatch = useDispatch();
  const handleFilterReset = () => {
    dispatch(unsetFilter(name));
    onChange([0, maxValue]);
  };

  return (
    <div className={classes.slider}>
      <div className={classes.sliderHeader}>
        <Typography className={classes.sliderHeaderText} id="range-slider" gutterBottom>
          Value range
        </Typography>
        <Button onClick={handleFilterReset}>
          Unset Filter
        </Button>
      </div>
      <MaterialSlider
        max={maxValue}
        min={0}
        value={value}
        onChange={handleChange}
        valueLabelDisplay="auto"
        aria-labelledby="range-slider"
      />
    </div>
  );
};

export default React.memo(Slider);
