import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Card, CardActions, CardContent, Container,
} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { injectIntl } from 'react-intl';
import { useHistory } from 'react-router-dom';
import { ERROR_CONTAINER_TYPE_ENUM } from 'constants.js';
import ActionButton from '../action-button';

const useStyles = makeStyles({
  wrapper: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: 25,
  },
  root: {
    minWidth: 275,
    maxWidth: 400,
    backgroundColor: '#921a1a',
  },
  optionalText: {
    fontSize: 20,
    color: '#e4d307',
  },
});

const ErrorContainer = ({
  error, optionalText, intl, type = ERROR_CONTAINER_TYPE_ENUM.reload, buttonText,
}) => {
  if (!error) {
    return null;
  }
  console.error(error);

  let handleOnClick = window ? () => window.location.reload() : null;
  if (type === ERROR_CONTAINER_TYPE_ENUM.goHome || !handleOnClick) {
    const history = useHistory();
    handleOnClick = () => history.push('/');
  }

  const classes = useStyles();
  return (
    <Container className={classes.wrapper}>
      <Card className={classes.root} variant="outlined">
        <CardContent>
          <Typography variant="h5" component="h2">
            {intl.formatMessage({ id: 'errorTitle' })}
          </Typography>
          {optionalText ? (
            <Typography className={classes.optionalText} color="textSecondary" gutterBottom>
              {optionalText}
            </Typography>
          ) : null}
        </CardContent>
        <CardActions>
          <ActionButton
            buttonText={buttonText || intl.formatMessage({ id: 'errorAction' })}
            onClick={handleOnClick}
          />
        </CardActions>
      </Card>
    </Container>
  );
};

export default injectIntl(ErrorContainer);
