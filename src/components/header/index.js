import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import {
  AppBar, Container, Toolbar, Typography,
} from '@material-ui/core';
import { STORAGE_THEME } from 'constants.js';
import { setDarkTheme } from 'store/actions/general';
import LocalStorage from 'service/LocalStorage';
import RightMenu from './right-header-part';

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
  },
  leftPart: {
    flexGrow: 1,
    color: 'inherit',
    textDecoration: 'none',
  },
  innerContent: {
    display: 'flex',
  },
}));

const Header = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  useEffect(() => {
    const theme = LocalStorage.get(STORAGE_THEME);
    try {
      const isDarkTheme = JSON.parse(theme);
      dispatch(setDarkTheme(isDarkTheme));
    } catch (e) {
      LocalStorage.remove(STORAGE_THEME);
    }
  }, [dispatch]);

  return (
    <>
      <div className={classes.root}>
        <AppBar color="inherit">
          <Toolbar variant="regular">
            <Container>
              <div className={classes.innerContent}>
                <NavLink className={classes.leftPart} to="/">
                  <Typography variant="h4" component="h1">Angular contributors</Typography>
                </NavLink>
                <RightMenu/>
              </div>
            </Container>
          </Toolbar>
        </AppBar>
        <Toolbar/>
      </div>
    </>
  );
};

export default Header;
