import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Menu, MenuItem, IconButton, Button,
} from '@material-ui/core';
import SettingsIcon from '@material-ui/icons/Settings';
import { FormattedMessage } from 'react-intl';
import { useDispatch } from 'react-redux';
import ThemeSwitcher from './menu-items/theme-switcher';
import LangSwitcher from './menu-items/lang-switcher';
import { unsetAllFilters } from '../../../store/actions/users';

const useStyles = makeStyles((theme) => ({
  menuItem: {
    borderBottom: `1px solid ${theme.palette.separator.main}`,
  },
  unsetFiltersButton: {
    height: '100%',
    width: '100%',
  },
}));

const RightHeaderPart = () => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const dispatch = useDispatch();
  const handleUnsetFilters = () => {
    dispatch(unsetAllFilters());
  };

  return (
    <div>
      <IconButton
        aria-label={<FormattedMessage id="settings"/>}
        aria-haspopup="true"
        size="small"
        onClick={handleMenu}
      >
        <SettingsIcon fontSize="large" />
      </IconButton>
      <Menu
        open={open}
        onClose={handleClose}
        anchorEl={anchorEl}
        getContentAnchorEl={null}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
        transformOrigin={{ vertical: 'top', horizontal: 'center' }}
        PaperProps={{
          style: {
            transform: 'translateX(16px) translateY(4px)',
            width: 300,
          },
        }}
      >
        <MenuItem className={classes.menuItem}>
          <LangSwitcher/>
        </MenuItem>
        <MenuItem className={classes.menuItem}>
          <ThemeSwitcher/>
        </MenuItem>
        <MenuItem className={classes.menuItem}>
          <Button className={classes.unsetFiltersButton} onClick={handleUnsetFilters}>
            <FormattedMessage id='unsetAllFiltersButton'/>
          </Button>
        </MenuItem>
      </Menu>
    </div>
  );
};

export default RightHeaderPart;
