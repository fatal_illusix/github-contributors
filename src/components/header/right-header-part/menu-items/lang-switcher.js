import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import LanguageIcon from '@material-ui/icons/Language';
import SelectBox from 'components/fields/select';
import { config, langOptions } from 'lang/config';
import { STORAGE_LANG, DEFAULT_LANG } from 'constants.js';
import LocalStorage from 'service/LocalStorage';
import { setLang } from 'store/actions/general';

const LangSwitcher = () => {
  const { lang } = useSelector((state) => state.general);
  const dispatch = useDispatch();

  const onChangeLanguage = useCallback((_, language) => {
    dispatch(setLang(language));
    LocalStorage.set(STORAGE_LANG, language);
  }, [dispatch]);

  useEffect(() => {
    if (config[lang]) {
      LocalStorage.set(STORAGE_LANG, lang);
      return;
    }
    onChangeLanguage(null, DEFAULT_LANG);
  }, [lang, onChangeLanguage]);

  return (
    <SelectBox
      Icon={LanguageIcon}
      fullWidth
      items={langOptions}
      onChange={onChangeLanguage}
      value={lang}
      label={<FormattedMessage id="language"/>}
    />
  );
};

export default LangSwitcher;
