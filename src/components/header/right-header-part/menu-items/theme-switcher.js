import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ThemeIcon from '@material-ui/icons/Brightness4Rounded';
import { FormattedMessage } from 'react-intl';
import { STORAGE_THEME } from 'constants.js';
import SwitchButton from 'components/fields/switch';
import { setDarkTheme } from 'store/actions/general';
import LocalStorage from 'service/LocalStorage';

const ThemeSwitcher = () => {
  const { isDarkTheme } = useSelector((state) => state.general);
  const dispatch = useDispatch();

  const onThemeSwitch = () => {
    LocalStorage.set(STORAGE_THEME, !isDarkTheme);
    dispatch(setDarkTheme(!isDarkTheme));
  };

  return (
    <SwitchButton onChange={onThemeSwitch} value={isDarkTheme} Icon={ThemeIcon} fullWidth>
      <FormattedMessage id="darkTheme"/>
    </SwitchButton>
  );
};

export default ThemeSwitcher;
