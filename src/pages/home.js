import React, { useEffect, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import DataTable from 'components/data-table';
import fillUsers from 'store/thunk/users';
import { ORG_NAME, REPO_NAME } from 'configs';
import { filterUsers } from 'store/actions/users';
import Loader from 'components/loader';
import ErrorContainer from '../components/error-container';

const fields = [
  { field: 'login', name: <FormattedMessage id='contributorName'/> },
  { field: 'contributions', name: <FormattedMessage id='contributions'/> },
  { field: 'followers', name: <FormattedMessage id='followers'/> },
  { field: 'publicRepos', name: <FormattedMessage id='repositories'/> },
  { field: 'publicGists', name: <FormattedMessage id='gists'/> },
];

const HomePage = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    dispatch(fillUsers(REPO_NAME, ORG_NAME));
  }, []);

  const [data, apiStatus, progress] = useSelector(({ users }) => {
    const { arrangedList, mappedList, apiStatus: apiState } = users;
    const lengths = {
      total: Object.values(mappedList).length,
      loaded: arrangedList.length,
    };
    return [arrangedList.map((userName) => mappedList[userName]), apiState, lengths];
  });

  const { filters: activeFilters, maxPerFields } = useSelector(({ users }) => users);

  const onFilterAccept = useCallback((name, value) => {
    const [min, max] = value;
    dispatch(filterUsers({
      filterBy: name,
      min,
      max,
    }));
  }, []);

  const onRowClick = useCallback(({ login }) => {
    history.push(`/user/${login}`);
  }, []);

  const { usersLoading, error } = apiStatus;
  const progressValue = (progress.loaded / progress.total) * 100;

  return (
    <>
      <Loader isLoading={usersLoading} progressValue={progressValue} error={error} centered >
        <DataTable
          data={data}
          fields={fields}
          onRowClick={onRowClick}
          onFilterAccept={onFilterAccept}
          maxPerFields={maxPerFields}
          activeFilters={activeFilters}
        />
      </Loader>
      <ErrorContainer error={error} />
    </>
  );
};

export default HomePage;
