import React, { useEffect } from 'react';
import UserCard from 'components/user-card';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { getSingleUser } from '../store/thunk/users';
import fillRepositories from '../store/thunk/repositories';
import DataTable from '../components/data-table';
import Loader from '../components/loader';
import ErrorContainer from '../components/error-container';

const dataListFields = [
  { field: 'name', name: <FormattedMessage id='repoName'/> },
  { field: 'id', name: <FormattedMessage id='repoId'/> },
  { field: 'description', name: <FormattedMessage id='repoDescription'/> },
  { field: 'size', name: <FormattedMessage id='repoSize'/> },
];

const UserPage = () => {
  const { user: userName } = useParams();
  const [
    user = {},
    usersApiStatus,
    repositories,
    reposApiStatus,
  ] = useSelector(({ users, repos }) => {
    const idList = repos.connectedWithUserList[userName] || [];
    const reposList = idList.map((id) => repos.mappedList[id]);

    return [
      users.mappedList[userName],
      users.apiStatus,
      reposList,
      repos.apiStatus,
    ];
  });
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getSingleUser(userName));
    dispatch(fillRepositories(userName));
  }, []);

  const { usersLoading, error: usersError } = usersApiStatus;
  const usersProgress = Number(!usersLoading) * 100;
  const { repositoriesLoading, error: repositoriesError } = reposApiStatus;
  let repositoriesProgress = (repositories.length / user.publicRepos) * 100;
  if (Number.isNaN(repositoriesProgress)) {
    repositoriesProgress = Number(!repositoriesLoading) * 100;
  }

  return (
    <>
      <Loader isLoading={usersLoading} progressValue={usersProgress} error={usersError} >
        <UserCard userObject={user} />
      </Loader>
      <Loader
        isLoading={repositoriesLoading}
        progressValue={repositoriesProgress}
        error={repositoriesError}
      >
        <DataTable
          data={repositories}
          fields={dataListFields}
          maxPerFields={{}}
          activeFilters={{}}
        />
      </Loader>
      <ErrorContainer error={usersError || repositoriesError} />
    </>
  );
};

export default UserPage;
