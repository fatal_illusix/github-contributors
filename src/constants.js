export const REMAINING_LIMIT_HEADER = 'x-ratelimit-remaining';
export const STORAGE_LANG = 'lang';
export const DEFAULT_LANG = 'en';
export const DEFAULT_PER_PAGE = 100;
export const STORAGE_THEME = 'isDarkTheme';

export const FILTERABLE_FIELDS = [
  'publicRepos',
  'publicGists',
  'followers',
  'contributions',
];

export const ERROR_CONTAINER_TYPE_ENUM = {
  reload: 'reload',
  goHome: 'goHome',
};
