import LocalStorage from 'service/LocalStorage';
import { STORAGE_LANG, DEFAULT_LANG } from 'constants.js';
import {
  SET_LANG,
  SET_DARK_THEME,
  SET_ACTIVE_POPUP,
} from 'store/actions/actionNames';

const initialState = {
  lang: LocalStorage.get(STORAGE_LANG)
    || (navigator && navigator.language && navigator.language.split('-')[0])
    || DEFAULT_LANG,
  isDarkTheme: false,
  activePopup: null,
};

const generalReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SET_LANG:
      return { ...state, lang: payload };
    case SET_DARK_THEME:
      return { ...state, isDarkTheme: payload };
    case SET_ACTIVE_POPUP:
      return { ...state, activePopup: payload };
    default:
      return state;
  }
};

export default generalReducer;
