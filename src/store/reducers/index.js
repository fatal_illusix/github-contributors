import { combineReducers } from 'redux';
import usersReducer from './users';
import generalReducer from './general';
import repositoriesReducer from './repositories';

const mainReducer = combineReducers({
  users: usersReducer,
  general: generalReducer,
  repos: repositoriesReducer,
});

export default mainReducer;
