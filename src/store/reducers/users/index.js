import {
  SET_USERS,
  UPDATE_USER,
  FETCH_USERS_START,
  FETCH_USERS_ERROR,
  FETCH_USERS_SUCCESS,
  FILTER_USERS,
  UNSET_FILTER,
  UNSET_ALL_FILTERS,
} from 'store/actions/actionNames';
import { FILTERABLE_FIELDS } from 'constants.js';

const initialState = {
  mappedList: {},
  arrangedList: [],
  apiStatus: {
    usersLoading: false,
    error: null,
    listFetchOccurred: false,
  },
  filters: {},
  maxPerFields: {},
};

FILTERABLE_FIELDS.forEach((field) => {
  initialState.maxPerFields[field] = 0;
});

const revertedFilterFunction = (user, filter) => {
  const { filterBy, min, max } = filter;
  const valueForFiltering = user[filterBy];
  return !valueForFiltering || valueForFiltering < min || valueForFiltering > max;
};

const filterListByFilters = (mappedList, filters) => {
  const filteredList = [];
  Object.values(mappedList).forEach((user) => {
    if (
      user.isNewUser
      || Object.values(filters)
        .some((currentFilter) => revertedFilterFunction(user, currentFilter))
    ) {
      return;
    }
    filteredList.push(user.login);
  });
  return filteredList;
};

const getUpdatedMappedList = (mappedList, payload) => {
  const updatedMap = {};
  payload.forEach(({ login, id, contributions }) => {
    if (mappedList[login]) {
      updatedMap[login] = {
        ...mappedList[login],
        contributions: contributions + mappedList[login].contributions,
      };
    } else {
      updatedMap[login] = { login, id, contributions };
    }
  });
  return updatedMap;
};

const addFiterAndApply = (state, action) => {
  const { filters, mappedList } = state;
  const { payload } = action;
  const { filterBy, min, max } = payload;
  if (!FILTERABLE_FIELDS.includes(filterBy)) {
    return state;
  }

  const newFilters = { ...filters };
  newFilters[filterBy] = {
    filterBy,
    min: min >= 0 ? min : 0,
    max,
  };

  return {
    ...state,
    filters: newFilters,
    arrangedList: filterListByFilters(mappedList, newFilters),
  };
};

const updateUser = (state, action) => {
  const { maxPerFields, mappedList, arrangedList } = state;
  const { payload } = action;
  const updatedMap = {};
  const updatedMaxPerFields = { ...maxPerFields };

  const isNewUser = !mappedList[payload.login];

  updatedMap[payload.login] = {
    ...(isNewUser ? { login: payload.login } : mappedList[payload.login]),
    avatarUrl: payload.avatar_url,
    name: payload.name,
    company: payload.company,
    location: payload.location,
    bio: payload.bio,
    hireable: payload.hireable,
    publicRepos: payload.public_repos,
    publicGists: payload.public_gists,
    followers: payload.followers,
    isNewUser,
  };

  FILTERABLE_FIELDS.forEach((field) => {
    const currentValue = updatedMap[payload.login][field];
    if (!updatedMap[payload.login][field]) {
      return;
    }
    if (currentValue <= updatedMaxPerFields[field]) {
      return;
    }
    updatedMaxPerFields[field] = currentValue;
  });

  const newArrangedList = [...arrangedList];
  if (!isNewUser && !arrangedList.find((login) => payload.login === login)) {
    newArrangedList.push(payload.login);
  }

  return {
    ...state,
    mappedList: { ...mappedList, ...updatedMap },
    maxPerFields: updatedMaxPerFields,
    arrangedList: newArrangedList,
  };
};

const usersReducer = (state = initialState, action) => {
  const { mappedList, apiStatus, filters } = state;
  const { type, payload } = action;
  let newFilters;

  switch (type) {
    case UNSET_ALL_FILTERS:
      if (apiStatus.usersLoading) {
        return state;
      }
      return {
        ...state,
        filters: {},
        arrangedList: Object.values(mappedList)
          .filter(({ isNewUser }) => !isNewUser)
          .map(({ login }) => login),
      };
    case UNSET_FILTER:
      newFilters = { ...filters };
      delete newFilters[payload];

      return {
        ...state,
        filters: newFilters,
        arrangedList: filterListByFilters(mappedList, newFilters),
      };
    case FILTER_USERS:
      return addFiterAndApply(state, action);
    case FETCH_USERS_START:
      return {
        ...state,
        apiStatus: {
          ...apiStatus,
          usersLoading: true,
          error: null,
        },
      };
    case FETCH_USERS_ERROR:
      return {
        ...state,
        apiStatus: {
          ...apiStatus,
          usersLoading: false,
          error: payload,
        },
      };
    case FETCH_USERS_SUCCESS:
      return {
        ...state,
        apiStatus: {
          ...apiStatus,
          usersLoading: false,
          error: null,
        },
      };
    case SET_USERS:
      return {
        ...state,
        mappedList: { ...mappedList, ...getUpdatedMappedList(mappedList, payload) },
        apiStatus: {
          ...apiStatus,
          listFetchOccurred: true,
        },
      };
    case UPDATE_USER:
      return updateUser(state, action);
    default:
      return state;
  }
};

export default usersReducer;
