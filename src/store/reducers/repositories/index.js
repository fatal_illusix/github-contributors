import {
  FETCH_REPOS_SUCCESS,
  FETCH_REPOS_ERROR,
  FETCH_REPOS_START,
  SET_REPOS,
} from 'store/actions/actionNames';

const initialState = {
  mappedList: {},
  connectedWithUserList: {},
  apiStatus: {
    repositoriesLoading: false,
    error: null,
  },
};

const setRepositories = (state, action) => {
  const { mappedList, connectedWithUserList } = state;
  const { payload } = action;
  const updatedMap = { ...mappedList };
  const { userLogin, repos } = payload;
  let connectedItem = { [userLogin]: [] };

  if (connectedWithUserList[userLogin]) {
    connectedItem = { [userLogin]: connectedWithUserList[userLogin] };
  }
  repos.forEach(({
    name, id, description, size,
  }) => {
    if (mappedList[id]) {
      return;
    }
    if (!connectedItem[userLogin].find((containedId) => containedId === id)) {
      connectedItem[userLogin].push(id);
    }
    updatedMap[id] = {
      name, id, description, size,
    };
  });
  return {
    ...state,
    mappedList: updatedMap,
    connectedWithUserList: {
      ...connectedWithUserList,
      ...connectedItem,
    },
  };
};

const usersReducer = (state = initialState, action) => {
  const { apiStatus } = state;
  const { type, payload } = action;
  switch (type) {
    case FETCH_REPOS_START:
      return {
        ...state,
        apiStatus: {
          ...apiStatus,
          repositoriesLoading: true,
          error: null,
        },
      };
    case FETCH_REPOS_ERROR:
      return {
        ...state,
        apiStatus: {
          ...apiStatus,
          repositoriesLoading: false,
          error: payload,
        },
      };
    case FETCH_REPOS_SUCCESS:
      return {
        ...state,
        apiStatus: {
          ...apiStatus,
          repositoriesLoading: false,
          error: null,
        },
      };
    case SET_REPOS:
      return setRepositories(state, action);
    default:
      return state;
  }
};

export default usersReducer;
