import API from 'service/api/github';
import * as actions from 'store/actions/repositories';
import { DEFAULT_PER_PAGE } from 'constants.js';

// eslint-disable-next-line consistent-return
const fillReposWithPagination = async (userName, dispatch, page = 1) => {
  let results;
  try {
    results = await API.getReposByUser(userName, page);
    dispatch(actions.setRepos({ userLogin: userName, repos: results }));
  } catch (e) {
    dispatch(actions.fetchReposError(e));
    throw e;
  }
  if (results.length === DEFAULT_PER_PAGE) {
    return fillReposWithPagination(userName, dispatch, page + 1);
  }
};

const fillRepositories = (userName) => async (dispatch, getState) => {
  if (getState().repos.apiStatus.usersLoading) {
    return;
  }
  dispatch(actions.fetchReposStart());

  await fillReposWithPagination(userName, dispatch);

  dispatch(actions.fetchReposSuccess());
};

export default fillRepositories;
