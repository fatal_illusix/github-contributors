import API from 'service/api/github';
import * as actions from 'store/actions/users';
import { DEFAULT_PER_PAGE } from 'constants.js';

// eslint-disable-next-line consistent-return
const setUsersWithPagination = async (repository, organizationName, dispatch, page = 1) => {
  let results = [];
  try {
    results = await API.getContributorsByOrgRepo(repository, organizationName, page);
    dispatch(actions.setUsers(results));
  } catch (e) {
    dispatch(actions.fetchUsersError(e));
    throw e;
  }
  if (results.length >= DEFAULT_PER_PAGE) {
    return setUsersWithPagination(repository, organizationName, dispatch, page + 1);
  }
};

export const fillUsers = (repository, organizationName) => async (dispatch, getState) => {
  const { users: { apiStatus, mappedList: listOnStart } } = getState();
  if ((apiStatus.usersLoading || Object.keys(listOnStart).length) && apiStatus.listFetchOccurred) {
    return;
  }
  dispatch(actions.fetchUsersStart());
  await setUsersWithPagination(repository, organizationName, dispatch);
  const { users: { mappedList } } = getState();

  const userLogins = Object.values(mappedList)
    .filter(({ isNewUser }) => !isNewUser)
    .map(({ login }) => login);

  for (let i = 0; i < userLogins.length; i += 1) {
    const currentUserLogin = userLogins[i];
    try {
      // eslint-disable-next-line no-await-in-loop
      const userData = await API.getUserData(currentUserLogin);
      dispatch(actions.updateUser(userData));
    } catch (e) {
      dispatch(actions.fetchUsersError(e));
      return;
    }
  }
  dispatch(actions.fetchUsersSuccess());
};

export const getSingleUser = (userName) => async (dispatch, getState) => {
  const { users } = getState();
  if (users.apiStatus.usersLoading || users.mappedList[userName]) {
    return;
  }
  dispatch(actions.fetchUsersStart());
  try {
    const userData = await API.getUserData(userName);
    dispatch(actions.updateUser(userData));
  } catch (e) {
    dispatch(actions.fetchUsersError(e));
    return;
  }
  dispatch(actions.fetchUsersSuccess());
};

export default fillUsers;
