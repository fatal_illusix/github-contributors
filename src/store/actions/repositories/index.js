import {
  FETCH_REPOS_SUCCESS,
  FETCH_REPOS_ERROR,
  FETCH_REPOS_START,
  SET_REPOS,
} from '../actionNames';

export const setRepos = (payload) => ({
  type: SET_REPOS,
  payload,
});

export const fetchReposSuccess = () => ({
  type: FETCH_REPOS_SUCCESS,
});

export const fetchReposError = (payload) => ({
  type: FETCH_REPOS_ERROR,
  payload,
});

export const fetchReposStart = () => ({
  type: FETCH_REPOS_START,
});
