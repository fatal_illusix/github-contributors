import {
  SET_ACTIVE_POPUP,
  SET_DARK_THEME,
  SET_LANG,
} from '../actionNames';

export const setDarkTheme = (payload) => ({
  type: SET_DARK_THEME,
  payload,
});

export const setActivePopup = (payload) => ({
  type: SET_ACTIVE_POPUP,
  payload,
});

export const setLang = (payload) => ({
  type: SET_LANG,
  payload,
});
