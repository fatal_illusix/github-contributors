import {
  UPDATE_USER,
  SET_USERS,
  FETCH_USERS_START,
  FETCH_USERS_ERROR,
  FETCH_USERS_SUCCESS,
  FILTER_USERS,
  UNSET_FILTER,
  UNSET_ALL_FILTERS,
} from '../actionNames';

export const updateUser = (payload) => ({
  type: UPDATE_USER,
  payload,
});

export const setUsers = (payload) => ({
  type: SET_USERS,
  payload,
});

export const fetchUsersStart = () => ({
  type: FETCH_USERS_START,
});

export const fetchUsersSuccess = () => ({
  type: FETCH_USERS_SUCCESS,
});

export const fetchUsersError = (payload) => ({
  type: FETCH_USERS_ERROR,
  payload,
});

export const filterUsers = (payload) => ({
  type: FILTER_USERS,
  payload,
});

export const unsetFilter = (payload) => ({
  type: UNSET_FILTER,
  payload,
});

export const unsetAllFilters = () => ({
  type: UNSET_ALL_FILTERS,
});
